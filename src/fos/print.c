/*
 * =====================================================================================
 *
 *       Filename:  print.c
 *
 *    Description:  Call the printf function
 *
 *        Version:  1.0
 *        Created:  10.08.2016 23:39:18
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include "print.h"

/* ===  FUNCTION  ======================================================================
*         Name:  print
*  Description:  
* =====================================================================================
                 */
void print(void)
{
  printf("Hello world!\n");
}                               /* -----  end of function print  ----- */
